import { createStore } from 'vuex'
import BoardModule from './BoardModule';
import PlayerModule from './PlayerModule';
export default createStore({
  state: {
    values : ["X", "O"],
    value : "",
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    BoardModule,
    PlayerModule
  }
})
