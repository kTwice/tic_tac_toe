import { createStore } from 'vuex';

export default createStore( {
    state: {
        player : false,
        players : [" Player X ", " Player O "],
    },
    getters: {
    },
    mutations: {
        selectedPlayer(playerValue) {
            if (playerValue) {
                this.value = this.values[1];
            } else {
                this.value = this.values[0];
            }
        },
        changePlayer(player) {
            if (player) {
                this.value = this.values[0];
            } else {
                this.value = this.values[1];
            }
            this.player = !player;
        },
    },
    actions: {
    }
})