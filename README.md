# tic_tac_toe

## Note du dev : kevKev
* la plupart du code est contenu dans le fichier GameTicTacToe.vue
* j'ai commencé à vouloir factoriser mon code de manière à le rendre plus visible et plus beau aussi
je n'ai pas reussi encore à bien comprendre et à exploiter le store avec vuex pour se faire.
* Pour le reste le jeu est fonctionnel, on peut choisir le joueur avec lequel on veut commencer (le jeu se joue à deux)
Ensuite, il suffit d'aligner trois mêmes formes pour marquer un point pour le joueur lié (yéyé ouh ouh)


## Commandes principales

### create project
```
vue create projectName
```

### Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### add commun dependancies
* router
```
vue add router
```

* store with vuex 
```
vue add vuex
``` 

* validator for form
```
npm install @vuelidate/core @vuelidate/validators
``` 